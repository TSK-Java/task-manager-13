package ru.tsc.kirillov.tm.api.service;

public interface IProjectTaskService {

    boolean bindTaskToProject(String projectId, String taskId);

    boolean unbindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
