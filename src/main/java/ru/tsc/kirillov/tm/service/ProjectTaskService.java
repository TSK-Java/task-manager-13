package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    private boolean bindTaskToProject(final String projectId, final String taskId, boolean isAdd) {
        if (projectId == null || projectId.isEmpty()) return false;
        if (taskId == null || taskId.isEmpty()) return false;
        if (!projectRepository.existsById(projectId)) return false;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return false;

        task.setProjectId(isAdd ? projectId : null);

        return true;
    }

    @Override
    public boolean bindTaskToProject(final String projectId, final String taskId) {
        return bindTaskToProject(projectId, taskId, true);
    }

    @Override
    public boolean unbindTaskToProject(final String projectId, final String taskId) {
        return bindTaskToProject(projectId, taskId, false);
    }

    @Override
    public void removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty())
            return;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task: tasks)
            taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
    }

}
