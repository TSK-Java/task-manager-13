package ru.tsc.kirillov.tm.controller;

import ru.tsc.kirillov.tm.api.controller.IProjectTaskController;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[Привязка задачи к проекту]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        final String taskId = TerminalUtil.nextLine();
        if (projectTaskService.bindTaskToProject(projectId, taskId))
            System.out.println("[Задача успешно добавлена к проекту]");
        else
            System.out.println("[Ошибка при привязке задачи к проекту]");
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[Отвязка задачи от проекта]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        final String taskId = TerminalUtil.nextLine();
        if (projectTaskService.unbindTaskToProject(projectId, taskId))
            System.out.println("[Задача успешно удалена из проекта]");
        else
            System.out.println("[Ошибка при отвязке задачи от проекта]");
    }

}
