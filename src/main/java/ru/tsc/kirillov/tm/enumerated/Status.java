package ru.tsc.kirillov.tm.enumerated;

public enum Status {

    NOT_STARTED("Не запущено"),
    IN_PROGRESS("Выполняется"),
    COMPLETED("Завершено");

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty())
            return null;
        for (Status status: values()) {
            if (status.name().equals(value))
                return status;
        }

        return null;
    }

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
